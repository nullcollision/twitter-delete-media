""" 
Download your Twitter archive, extract tweet.js to the same folder as the script, run it, done!
"""

import sys
import os
import json
import tweepy #https://github.com/tweepy/tweepy

#Twitter API credentials
consumer_key = "xxxxxxx"
consumer_secret = "xxxxxxx"
access_key = "xxxxx"
access_secret = "xxxxxxxx"

counter = 0

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_key, access_secret)
api = tweepy.API(auth)


if os.path.isfile("tweet.js") is False: 
    error("tweet.js is missing!")
    sys.exit(1)

fp = open("tweet.js","r", encoding="utf-8")
myjson = json.load(fp)


if "all" in sys.argv:
    print("Deleting ALL tweets.")

print("Tweets to delete:", len(myjson))
for item in myjson:
    if "all" not in sys.argv:
        if 'media' in item["entities"]:
            try:
                api.destroy_status(item["id_str"])
                print("Deleted", counter, "images")
                counter = counter + 1
            except: # Image already deleted
                print("Already deleted.")
                pass
    else: # Delete all tweets
        try:
            api.destroy_status(item["id_str"])
            print("Deleted tweet (", str(counter) , "/", str(len(myjson)) + ")")
            counter = counter + 1
        except:
            print("Already deleted. (", str(counter) , "/", str(len(myjson)) + ")")
            counter = counter + 1

            pass

print("Deleted count:", counter)